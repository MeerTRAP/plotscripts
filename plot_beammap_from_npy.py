#!/usr/env/python

import numpy as np
import matplotlib.pyplot as plt
import glob
import sys

if (len(sys.argv) != 5):
    print "Incorrect number of arguments"
    print "Usage: python plot_beammap.py <nchans> <beam index 1> <beam index2> <grid dimension>"  
    exit()

nchans = int(sys.argv[1])
beamid1= int(sys.argv[2])
beamid2 = int(sys.argv[3])
dim = int(sys.argv[4])

x = glob.glob("*.npy") 

x = sorted(x,key=lambda z:int(z.replace("beammap","").replace("-","").replace(".npy","")))

data = np.array([np.load(i) for i in x]) 

ind = np.arange(0, len(data))
data_fin = np.concatenate(([data[i] for i in ind]), axis = 0)

plt.imshow(data_fin.sum(axis=1)[beamid1:beamid2].reshape(dim,dim),aspect='auto')
plt.show()

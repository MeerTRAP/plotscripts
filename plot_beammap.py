#!/usr/env/python

import numpy as np
import matplotlib.pyplot as plt
import glob
import sys

if (len(sys.argv) != 6):
    print "Incorrect number of arguments"
    print "Usage: python plot_beammap.py <nchans> <beam index 1> <beam index2> <grid dimension> <base fil name>"  
    exit()

nchans = int(sys.argv[1])
beamid1= int(sys.argv[2])
beamid2 = int(sys.argv[3])
dim = int(sys.argv[4])
basename = str(sys.argv[5])

def read(fname):                                                           
    with open(fname, "r") as f:
         f.seek(277)
         p = np.fromfile(f, dtype='ubyte')
         p = p.reshape(p.size/nchans,nchans).mean(axis=0)
    return p

x = glob.glob("*"+basename) 

x = sorted(x,key=lambda z:int(z.replace("beamcb_","").replace(basename,"")))

data = np.array([read(i) for i in x]) 

ind = np.arange(0, len(data))
data_fin = np.concatenate(([data[i] for i in ind]), axis = 0)

plt.imshow(data_fin[beamid1:beamid2].reshape(dim,dim),aspect='auto')
plt.show()
